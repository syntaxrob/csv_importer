﻿using CsvHelper;
using CsvHelper.Configuration.Attributes;
using System;
using System.Data;
using System.IO;
using System.Linq;

//https://joshclose.github.io/CsvHelper/getting-started/

namespace CSVImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var sampleLocation = "F:/SpeechToText/CSVImporter/CSVImporter/Sample.csv";
            using (var reader = new StreamReader(sampleLocation))
            using (var csv = new CsvReader(reader))
            {
                var records = csv.GetRecords<DataSet>();
                var data = records.ToList();
                foreach (var item in data)
                {
                    if (item.FurtherInfo != "")
                    {
                        Console.WriteLine(item.Name + ": " + item.Age + ". " + item.Description + ", " + item.FurtherInfo);
                    }
                    else
                    {
                        Console.WriteLine(item.Name + ": " + item.Age + ". " + item.Description);
                    }

                }
                Console.ReadLine();
            }

            var footballLocation = "C:/Users/pro/Downloads/football-competition-offence.csv";
            using (var reader = new StreamReader(footballLocation))
            using (var csv = new CsvReader(reader))
            {
                var records = csv.GetRecords<FootballOffences>();
                var data = records.ToList();
                foreach (var item in data)
                {
                    var x = int.Parse(item.PitchIncursion);
                    var y = int.Parse(item.MissileThrowing);
                    var z = x + y;
                    Console.WriteLine(z);
                }
                Console.ReadLine();
            }
        }

        public class DataSet
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string FurtherInfo { get; set; }
            public int Age { get; set; }
        }

        public class FootballOffences
        {
            public string Competition { get; set; }
            public string Total { get; set; }
            public string ViolentAndPublic { get; set; }
            public string Disorder { get; set; }
            public string MissileThrowing { get; set; }
            public string RacistIndecentChanting { get; set; }
            public string PitchIncursion { get; set; }
            public string AlcoholOffences { get; set; }
            public string TicketTouting { get; set; }
            public string PossessionOfOffensiveWeapon { get; set; }
            public string BreachOfBanningOrder { get; set; }
        }
    }
}

