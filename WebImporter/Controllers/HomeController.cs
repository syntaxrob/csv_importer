﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows;
using WebImporter.Helpers;
using WebImporter.Models;

namespace WebImporter.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var fileName = "NewFile.csv";
            var existingFile = Path.Combine(Server.MapPath("~/Uploads"), fileName);

            if (System.IO.File.Exists(existingFile))
            {
                System.IO.File.Delete(existingFile);
            }

            MessageBox.Show(existingFile);
            MessageBox.Show(System.IO.File.Exists(existingFile) ? "File exists." : "File does not exist.");

            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                var fileName = "NewFile.csv";
                var path = Path.Combine(Server.MapPath("~/Uploads"), fileName);
                file.SaveAs(path);

                return RedirectToAction("Mappings");

                //using (var reader = new StreamReader(path))
                //using (var csv = new CsvReader(reader))
                //{
                //    //Use custom mapping
                //    csv.Configuration.RegisterClassMap<GenovoDataSetMap>();
                //    var records = csv.GetRecords<GenovoDataSet>();

                //    //Get Imported Headers
                //    //csv.Read();
                //    //csv.ReadHeader();
                //    //var importedHeaderRow = csv.Context.HeaderRecord.ToList();

                //    var data = records.ToList();
                //    if (data.Count > 0)
                //    {
                //        if (data.Count == 1)
                //        {
                //            ViewBag.SuccessMessage = "👍 Upload Successful. " + data.Count() + " new record added";
                //        }
                //        else
                //        {
                //            ViewBag.SuccessMessage = "👍 Upload Successful. " + data.Count() + " new records added";
                //        }
                //    }
                //    else if (data.Count == 0)
                //    {
                //        ViewBag.SuccessMessage = "Upload Failed";
                //    }

                //    return View(data);
                //}
            }

            return RedirectToAction("Index");
        }

        public ActionResult Mappings()
        {
            var headers = Data.GetHeaders().ToList();

            IEnumerable<SelectListItem> items = headers.Select((head,index) =>
            {
                return new SelectListItem
                {
                    Value = head,
                    Text = index.ToString()

                };
            });

            ViewBag.dropdowncontents = new SelectList(items, "Text", "Value");

            return View();
        }

        [HttpPost]
        public ActionResult Mappings(MappingViewModel model)
        {
            //https://stackoverflow.com/questions/57664783/how-to-drive-mapper-class-through-data

            //new csvMapping { PropertyName = "UniqueId", CsvHeadeName = ArrayOfHeaders[model.UniqueId]}

            return View(model);
        }
    }
}