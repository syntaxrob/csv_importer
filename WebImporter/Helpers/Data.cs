﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebImporter.Helpers
{
    public static class Data
    {
        public static string[] GetHeaders()
        {
            //var path = "D:\\Education\\csv_importer\\WebImporter\\Uploads\\NewFile.csv";
            var path = "E:\\Genovo\\D\\Education\\csv_importer\\WebImporter\\Uploads\\NewFile.csv";
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader))
            {
                csv.Read();
                csv.ReadHeader();
                var importedHeaderRow = csv.Context.HeaderRecord;

                return importedHeaderRow;
            }
        }
    }
}