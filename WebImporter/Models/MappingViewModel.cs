﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebImporter.Models
{
    public class MappingViewModel
    {
        [Required]
        [DisplayName("Unique Id")]
        public string UniqueId { get; set; }

        
        public int RiskRating { get; set; }

        [Required]
        public int Investment { get; set; }
    }
}