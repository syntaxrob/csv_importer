﻿using System.ComponentModel.DataAnnotations;

namespace WebImporter.Models
{
    public class GenovoDataSet
    {
        public int Id { get; set; }

        [Display(Name = @"Unique Identifier")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = @"Investment")]
        public string Investment { get; set; }

        [Display(Name = @"Investment Objective")]
        public string InvestmentObjective { get; set; }

        [Display(Name = @"Sector")]
        public string Sector { get; set; }

        [Display(Name = @"Risk Rating")]
        public string RiskRating { get; set; }

        [Display(Name = @"Initial Charge Percentage")]
        public string InitialChargePercentage { get; set; }

        [Display(Name = @"Annual Management Charge Percentage")]
        public string AnnualManagementChargePercentage { get; set; }

        [Display(Name = @"Total Expense Percentage")]
        public string TotalExpensePercentage { get; set; }

        [Display(Name = @"Ongoing Charges Percentage")]
        public string OngoingChargesPercentage { get; set; }

        [Display(Name = @"DFM Percentage")]
        public string DFMPercentage { get; set; }

        [Display(Name = @"Transition Charge Percentage")]
        public string TransitionChargePercentage { get; set; }

        [Display(Name = @"Incidental Costs")]
        public string IncidentalCosts { get; set; }

        [Display(Name = @"Further Information")]
        public string FurtherInfo { get; set; }
    }
}